@extends('layouts.app')

@section('content')

    <div class="page-content">
        <div class="container-fluid">
            <header class="section-header">
                <div class="tbl">
                    <div class="tbl-row">
                        <div class="tbl-cell">
                            <h3>Roles</h3>
                            <ol class="breadcrumb breadcrumb-simple">
                                <li><a href="{{ route('home') }}">Inicio</a></li>
                                <li><a href="{{ route('roles.index') }}">Roles</a></li>
                                <li><a href="#!">Agregar Rol</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </header>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> Hubo algunos problemas con los datos ingresados.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Nombre:</strong>
                                            {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Permisos:</strong>
                                            <br>
                                            <br/>
                                            @foreach($permission as $value)
                                                <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                                                    {{ $value->name }}</label>
                                                <br/>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                        <a href="{{ route('roles.index') }}" class="btn btn-warning">
                                            <i class="ti-back-left"></i> Atrás</a>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 text-left">
                                        <button type="submit" class="btn btn-primary">Guardar</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

