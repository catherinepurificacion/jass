@extends('layouts.app')
@section('content')

    <div class="page-content">
        <div class="container-fluid">
            <header class="section-header">
                <div class="tbl">
                    <div class="tbl-row">
                        <div class="tbl-cell">
                            <h3>Roles</h3>
                            <ol class="breadcrumb breadcrumb-simple">
                                <li><a href="{{ route('home') }}">Inicio</a></li>
                                <li><a href="{{ route('roles.index') }}">Roles</a></li>
                                <li><a href="#!">Ver Rol</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </header>
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong><b>Nombre:</b></strong>
                                        {{ $role->name }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong><b>Permisos:</b></strong>
                                        @if(!empty($rolePermissions))
                                            @foreach($rolePermissions as $v)
                                                <label class="label label-success">{{ $v->name }},</label>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>

                                <div class="d-flex justify-content-end items-center">
                                    <a href="{{ route('roles.index') }}"
                                       class="btn btn-success btn-round btn-sm ">
                                        <i class="ti-back-left"></i> Atrás</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection