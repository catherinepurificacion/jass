@extends('layouts.app')

@section('content')

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6">
                    <div class="chart-statistic-box">
                        <div class="chart-txt">
                            <div class="chart-txt-top">
                                <p><span class="unit">$</span><span class="number">1540</span></p>
                                <p class="caption">Week income</p>
                            </div>
                            <table class="tbl-data">
                                <tr>
                                    <td class="price color-purple">120$</td>
                                    <td>Orders</td>
                                </tr>
                                <tr>
                                    <td class="price color-yellow">15$</td>
                                    <td>Investments</td>
                                </tr>
                                <tr>
                                    <td class="price color-lime">55$</td>
                                    <td>Others</td>
                                </tr>
                            </table>
                        </div>
                        <div class="chart-container">
                            <div class="chart-container-in">
                                <div id="chart_div"></div>
                                <header class="chart-container-title">Income</header>
                                <div class="chart-container-x">
                                    <div class="item"></div>
                                    <div class="item">tue</div>
                                    <div class="item">wed</div>
                                    <div class="item">thu</div>
                                    <div class="item">fri</div>
                                    <div class="item">sat</div>
                                    <div class="item">sun</div>
                                    <div class="item">mon</div>
                                    <div class="item"></div>
                                </div>
                                <div class="chart-container-y">
                                    <div class="item">300</div>
                                    <div class="item"></div>
                                    <div class="item">250</div>
                                    <div class="item"></div>
                                    <div class="item">200</div>
                                    <div class="item"></div>
                                    <div class="item">150</div>
                                    <div class="item"></div>
                                    <div class="item">100</div>
                                    <div class="item"></div>
                                    <div class="item">50</div>
                                    <div class="item"></div>
                                </div>
                            </div>
                        </div>
                    </div><!--.chart-statistic-box-->
                </div><!--.col-->
                <div class="col-xl-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <article class="statistic-box red">
                                <div>
                                    <div class="number">26</div>
                                    <div class="caption">
                                        <div>Open tickets</div>
                                    </div>
                                    <div class="percent">
                                        <div class="arrow up"></div>
                                        <p>15%</p>
                                    </div>
                                </div>
                            </article>
                        </div><!--.col-->
                        <div class="col-sm-6">
                            <article class="statistic-box purple">
                                <div>
                                    <div class="number">12</div>
                                    <div class="caption">
                                        <div>Closes tickets</div>
                                    </div>
                                    <div class="percent">
                                        <div class="arrow down"></div>
                                        <p>11%</p>
                                    </div>
                                </div>
                            </article>
                        </div><!--.col-->
                        <div class="col-sm-6">
                            <article class="statistic-box yellow">
                                <div>
                                    <div class="number">104</div>
                                    <div class="caption">
                                        <div>New clients</div>
                                    </div>
                                    <div class="percent">
                                        <div class="arrow down"></div>
                                        <p>5%</p>
                                    </div>
                                </div>
                            </article>
                        </div><!--.col-->
                        <div class="col-sm-6">
                            <article class="statistic-box green">
                                <div>
                                    <div class="number">29</div>
                                    <div class="caption">
                                        <div>Here is an example of a long name</div>
                                    </div>
                                    <div class="percent">
                                        <div class="arrow up"></div>
                                        <p>84%</p>
                                    </div>
                                </div>
                            </article>
                        </div><!--.col-->
                    </div><!--.row-->
                </div><!--.col-->
            </div><!--.row-->
        </div><!--.container-fluid-->
    </div><!--.page-content-->

    <div class="control-panel-container">
        <ul>
            <li class="tasks">
                <div class="control-item-header">
                    <a href="#" class="icon-toggle">
                        <span class="caret-down fa fa-caret-down"></span>
                        <span class="icon fa fa-tasks"></span>
                    </a>
                    <span class="text">Task list</span>
                    <div class="actions">
                        <a href="#">
                            <span class="fa fa-refresh"></span>
                        </a>
                        <a href="#">
                            <span class="fa fa-cog"></span>
                        </a>
                        <a href="#">
                            <span class="fa fa-trash"></span>
                        </a>
                    </div>
                </div>
                <div class="control-item-content">
                    <div class="control-item-content-text">You don't have pending tasks.</div>
                </div>
            </li>
            <li class="sticky-note">
                <div class="control-item-header">
                    <a href="#" class="icon-toggle">
                        <span class="caret-down fa fa-caret-down"></span>
                        <span class="icon fa fa-file"></span>
                    </a>
                    <span class="text">Sticky Note</span>
                    <div class="actions">
                        <a href="#">
                            <span class="fa fa-refresh"></span>
                        </a>
                        <a href="#">
                            <span class="fa fa-cog"></span>
                        </a>
                        <a href="#">
                            <span class="fa fa-trash"></span>
                        </a>
                    </div>
                </div>
                <div class="control-item-content">
                    <div class="control-item-content-text">
                        StartUI – a full featured, premium web application admin dashboard built with Twitter Bootstrap 4,
                        JQuery and CSS
                    </div>
                </div>
            </li>
            <li class="emails">
                <div class="control-item-header">
                    <a href="#" class="icon-toggle">
                        <span class="caret-down fa fa-caret-down"></span>
                        <span class="icon fa fa-envelope"></span>
                    </a>
                    <span class="text">Recent e-mails</span>
                    <div class="actions">
                        <a href="#">
                            <span class="fa fa-refresh"></span>
                        </a>
                        <a href="#">
                            <span class="fa fa-cog"></span>
                        </a>
                        <a href="#">
                            <span class="fa fa-trash"></span>
                        </a>
                    </div>
                </div>
                <div class="control-item-content">
                    <section class="control-item-actions">
                        <a href="#" class="link">My e-mails</a>
                        <a href="#" class="mark">Mark visible as read</a>
                    </section>
                    <ul class="control-item-lists">
                        <li>
                            <a href="#">
                                <h6>Welcome to the Community!</h6>
                                <div>Hi, welcome to the my app...</div>
                                <div>
                                    Message text
                                </div>
                            </a>
                            <a href="#" class="reply-all">Reply all</a>
                        </li>
                        <li>
                            <a href="#">
                                <h6>Welcome to the Community!</h6>
                                <div>Hi, welcome to the my app...</div>
                                <div>
                                    Message text
                                </div>
                            </a>
                            <a href="#" class="reply-all">Reply all</a>
                        </li>
                        <li>
                            <a href="#">
                                <h6>Welcome to the Community!</h6>
                                <div>Hi, welcome to the my app...</div>
                                <div>
                                    Message text
                                </div>
                            </a>
                            <a href="#" class="reply-all">Reply all</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="add">
                <div class="control-item-header">
                    <a href="#" class="icon-toggle no-caret">
                        <span class="icon fa fa-plus"></span>
                    </a>
                </div>
            </li>
        </ul>
        <a class="control-panel-toggle">
            <span class="fa fa-angle-double-left"></span>
        </a>
    </div>
@endsection
