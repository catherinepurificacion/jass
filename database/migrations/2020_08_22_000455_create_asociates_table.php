<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsociatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asociates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dni_ruc', 11)->unique();
            $table->string('nombres');
            $table->string('apellidos')->nullable();
            $table->string('direccion')->nullable();
            $table->string('telefono')->nullable();
            $table->string('email')->nullable();
            $table->string('fec_inscrip')->nullable();
            $table->string('observacion')->nullable();
            $table->string('foto')->nullable();
            $table->integer('estado')->default(1);
            $table->bigInteger('document_type_id')->unsigned();
            $table->foreign('document_type_id')->references('id')->on('document_types');
            $table->bigInteger('asociate_type_id')->unsigned();
            $table->foreign('asociate_type_id')->references('id')->on('asociate_types');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asociates');
    }
}
