pp <?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'Listar-rol',
            'Crear-rol',
            'Editar-rol',
            'Eliminar-rol',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'asociate-list',
            'asociate-create',
            'asociate-edit',
            'asociate-delete',
            'asociate_type-list',
            'asociate_type-create',
            'asociate_type-edit',
            'asociate_type-delete'
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
        /* Insertamos los datos del usuario */
        $role = Role::create(['name' => 'Administrador']);
        $permissions = Permission::pluck('id', 'id')->all();
        $role->syncPermissions($permissions);
        $users = array(
            ['name' => 'Catherine', 'email' => 'catherinepurificacion@gmail.com', 'password' => bcrypt('catherine'), 'email_verified_at' => '2020-08-12 11:50:06'],
        );
        foreach ($users as $user) {
            $user = User::create($user);
            $user->assignRole([$role->id]);
        }
    }
}
