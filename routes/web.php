<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::resource('roles', 'RoleController');
    Route::post('roles/all', 'RoleController@roles_all')->name('roles.all');
    Route::resource('users', 'UserController');
    Route::post('users/all', 'UserController@users_all')->name('users.all');

} );